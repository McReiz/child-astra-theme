<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

       							<?php foreach ( $reviews as $i => $review ):
						$rating = intval( get_comment_meta( $review->comment_ID, 'rating', true ) );
						$subject = get_comment_meta( $review->comment_ID, 'subject', true );

						$order_id = intval( get_comment_meta( $review->comment_ID, 'ivole_order', true ) );
						$country = get_comment_meta( $review->comment_ID, 'ivole_country', true );
						$country_code = null;
						if( $country_enabled && is_array( $country ) && isset( $country['code'] ) ) {
						$country_code = $country['code'];
						}
						?>
				 <div class="rev column col-xl-3 col-lg-4 col-md-6 col-sm-12">
                 <a class="rev-block" href="#"><img style="margin-top: 40px;" src="https://smartmikesillymike.com/wp-content/themes/astra/assets/images/icons/rev-sidebar-widget.png" alt="" /></a>
					<div class="testimonial-block">
                 
						<div class="inner-box ivole-review-card">
								<div class="upper-info top-row">
									<div class="rating">
										<div class="crstar-rating">
											<span  style="width:<?php echo ($rating / 5) * 100; ?>%;">
											</span>  
										</div>
									</div>
                                    <span class="date"><?php printf( _x( '%s ago', '%s = human-readable time difference', IVOLE_TEXT_DOMAIN ), human_time_diff( mysql2date( 'U', $review->comment_date, true ), current_time( 'timestamp' ) ) ); ?></span>
								</div>
								<div class="content">
                                    <span class="name">
											<?php
											echo get_comment_author( $review );
											
											?>
									</span>
                                    <p class="review-subject"><strong><?php echo $subject ?></strong></p>
                                    <p><?php echo wp_trim_words($review->comment_content, 50,'.....') ; ?></p>
                                </div>
							
					</div>
					</div>
				</div>	
					<?php endforeach; ?>