
<?php 
class class_erz_snippets{
    
    static function select_input_cat($name, $options, $default_value){
        
        $default_value = ($default_value != null || $default_value != false || $default_value != 0) ? $default_value : 'none'; 
        
        $array = array(
            "none" => "none"
        );
        
        foreach($options as $option){
            $a = array($option->name => $option->term_id);
            $array = array_merge($array, $a);
        }
        
        ?>
            <select name="<?php echo $name ?>">
                <?php
                foreach($array as $key => $value){
                    $default = ($value == $default_value) ? ' selected ' : '';
                    ?><option value="<?php echo $value ?>"<?php echo $default ?>><?php echo $key ?></option><?php
                }?>
            </select>
        <?php      
    }
}


?>