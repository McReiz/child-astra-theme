<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

global $product;
global $post;

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<style>
        
        
        .woocommerce-breadcrumb {
            display: none !important;
        }
      
        
.woocommerce .select2-container .select2-selection--single, .woocommerce select, .woocommerce-page .select2-container .select2-selection--single, .woocommerce-page select {
            
position: relative;
margin: 3px auto;
min-width: 150px;

border-radius: 6px;
border: 2px solid #247ff9;
padding: 5px 10px;
line-height: 20px;
font-size: 14px;
}
.woocommerce-checkout p.woocommerce-invalid-required-field span.error {
   color: #e2401c;
   display: block !important;
   font-weight: bold;
}
.qib-container:not(#qib_id):not(#qib_id) {
    display: none !important;
}

.row.custom-row.no-gutters.ivole-reviews-grid .rev-block {
    display: none !important;
}
/*section.package-section .rev {
    margin-left: 51px;
}*/
form.cart button[type='submit']:not(#qib_id):not(#qib_id):not(_):not(_) {
    height: 50px !important;
    text-align: center;
}
.ivole-reviews-grid .ivole-review-card .top-row .rating .crstar-rating, .cr-reviews-slider .ivole-review-card .top-row .rating .crstar-rating {
    color: #6bba70;
    font-size: 1.5em;
    margin-top: 0px;
}
.woocommerce-checkout p.woocommerce-invalid-required-field span.error {
   color: #e2401c;
   display: block !important;
   font-weight: bold;
}
.single_variation_wrap{display:none !important;}
        </style>

<script>
    (function ($){
        $(".sliding-link").click(function(e) {
            e.preventDefault();
            var aid = $(this).attr("href");
            $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
        });
    })(jQuery);
</script>
  <!-- Package Section -->
<section class="package-section ivole-reviews-grid">
    <div class="diagonal"></div>
    <div class="auto-container">
        <div class="row no-gutters">
            <?php
                $short_description = apply_filters( 'woocommerce_short_description', $post->post_content );
                    if ( ! $short_description ) {
                        return;
                    }
                ?>
                <?php echo $short_description; // WPCS: XSS ok. ?> 
        </div>
        <form class="variations_form cart padding-bottom" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
            <?php do_action( 'woocommerce_before_variations_form' ); ?>
            <?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
                <p class="stock out-of-stock"><?php echo esc_html( apply_filters( 'woocommerce_out_of_stock_message', __( 'This product is currently out of stock and unavailable.', 'woocommerce' ) ) ); ?></p>
            <?php else : ?>
                <?php $count = 0; ?>
                <?php $countr = 0; ?>
                <div class="table-outer">
                    <div class="package-table">
                        <div class="title"></div>
                        <div class="row no-gutters">
                            <div class="left-column col-lg-8 col-md-8 col-sm-12">
                                <ul class="package-list">
                                    <li class="empty title"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/left-arrow7.png"> Click the icon to expand for more details! </li>
                                    <?php foreach ( $attributes as $attribute_name => $options ) : ?>
                                        <?php ++$count%2; ?>                                   
                                        <li data-id="0<?php echo $count;?>">
                                            <div class="opener"><?php echo wc_attribute_label( $attribute_name ); ?></div>
                                            <div class="slide-block">
                                                <p>Before we submit your paperwork to the state, we will verify that your chosen business name is available. If your business name is unavailable, we will promptly contact you for alternate names.</p>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>                        
                                    <li class="total"><strong>TOTAL:</strong></li>
                                </ul>   
                            </div>
                            <div class="right-column col-lg-4 col-md-4 col-sm-12">                             
                                <ul class="package-inc">
                                    <li class="empty"></li>
                                    <?php foreach ( $attributes as $attribute_name => $options ) : ?>
                                        <?php ++$countr%2; ?>                                   
                                        <li data-value="0<?php echo $countr;?>">
                                            <?php
                                                wc_dropdown_variation_attribute_options( array(
                                                    'options'   => $options,
                                                    'attribute' => $attribute_name,
                                                    'product'   => $product,
                                                    'desc_tip' => 'true',
                                                    'custom_attributes' => array( 'required' => 'required' ),
                                                    'description' => __( 'Enter wholesaler price here.', 'woocommerce' )
                                                    )
                                                );
                                                echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';
                                            ?>
                                        </li>
                                    <?php endforeach; ?>            
                                    <li class="price"><?php echo $product->get_price_html(); ?></li>
                                    <li class="continue" id="scrolladdtocart">
                                        <?php do_action( 'woocommerce_single_variation' ); ?>
                                    </li>
                                </ul>
                                <div class="single_variation_wrap">
                                    <?php
                                        /**
                                        * Hook: woocommerce_before_single_variation.
                                        */
                                        do_action( 'woocommerce_before_single_variation' );

                                        /**
                                        * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
                                        *
                                        * @since 2.4.0
                                        * @hooked woocommerce_single_variation - 10 Empty div for variation data.
                                        * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
                                        */
                                        do_action( 'woocommerce_single_variation' );

                                        /**
                                        * Hook: woocommerce_after_single_variation.
                                        */
                                        do_action( 'woocommerce_after_single_variation' );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php do_action( 'woocommerce_after_variations_form' ); ?>
        </form>
    <?php
    do_action( 'woocommerce_after_add_to_cart_form' );
    ?>
    </div>
    </div>
</section>
<!-- End Package Section -->


 <!-- Features Section -->
    <section class="features-section">
        <div class="diagonal"></div>
        <div class="sec-title text-center">
            <h2>HOW IT WORKS</h2>
            <span class="sub-title">Fase and easy</span>
        </div>
        <div class="row">
            <!-- Service Block -->
            <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-clock.svg" alt=""></div>
                    <h4>Add to cart & purchase</h4>
                    <div class="text">99% of applicants get money just 1 business day after accepting their loans*</div>
                </div>
            </div>

            <!-- Service Block -->
            <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-bag.svg" alt=""></div>
                    <h4>Tell us about your business</h4>
                    <div class="text">99% of applicants get money just 1 business day after accepting their loans*</div>
                </div>
            </div>

            <!-- Service Block -->
            <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-calendar.png" alt=""></div>
                    <h4>we do the best</h4>
                    <div class="text">99% of applicants get money just 1 business day after accepting their loans*</div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Features Section -->
 <!-- Services Section -->
    <section class="services-section">
        <div class="diagonal"></div>
        <div class="sec-title">
            <h2>FILE LLC IN TAXES<br> FOR ANY BUSINESS</h2>
        </div>
        <div class="outer-box clearfix">
            <!-- Service Block -->
            <div class="column">
                <div class="service-block block-one" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/moving_loans.jpg);">
                    <div class="text-box"><h3>Moving Loans</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
                <div class="service-block block-two" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/credit_card_consolidation.jpg);">
                    <div class="text-box"><h3>Credit Card Consolidation</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
            </div>

            <!-- Service Block -->
            <div class="column">
                <div class="service-block block-two" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/home_improvement.jpg);">
                    <div class="text-box"><h3>Home Improvement</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
                <div class="service-block block-one" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/debt_consolidation.jpg);">
                    <div class="text-box"><h3>Debt Consolidation</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
            </div>

            <!-- Service Block -->
            <div class="column">
                <div class="service-block block-one" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/medical_loans.jpg);">
                    <div class="text-box"><h3>Medical Loans</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
                <div class="service-block block-two" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/wedding_loans.jpg);">
                    <div class="text-box"><h3>Wedding Loans</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services Section -->
 <!-- Testimonials Section -->
 
 
   <!-- Testimonials Section -->
   <section class="testimonials-section">
            <div class="diagonal"></div>
            <div class="sec-title text-right">
                <h2>WHAT OUR NEW BUSINESS <br>OWNERS SAY</h2>
            </div>

            <div class="outer-box">
                <div class="upper-title">
                    <h4>Excellent</h4>
                    <div class="rating-star">
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </div>
                </div>

                <!-- Business Info -->
                <div class="businessinfo">
              <?php
              $rating  = $product->get_average_rating();
             $count   = $product->get_rating_count();

//echo wc_get_rating_html( $rating, $count );
               ?>
                    <a href="#"><span>Rated  <b><?php echo $rating; ?></b> out of 5 based on <b class="text_underline"><?php echo $count; ?> reviews </b></a><span class="palito-separador"> | </span><a href="#" class="show-review-form"><b class="text_underline">Add you Review</b></a></span>
                </div>

                <div class="wrapper-add-reviews">
                    <?php do_action('caber_reviews_variation') ?>
                </div>

                <div class="wrapper-reviews">
                    <div class="row custom-row no-gutters ivole-reviews-grid">
                       
                                <?php   echo do_shortcode( '[cusrev_reviews_grid count="100" products="'.$product->get_id().'"]' ); ?>
                             
                    </div>
                </div>
            </div>

            <div class="btn-box">
                <a href="#scrolladdtocart"  class="sliding-link theme-btn btn-style-one"><span class="btn-title">Get Started</span></a>
            </div>
        </section>
        <!-- End Testimonials Section -->
    <!-- End Testimonials Section -->
    <?php 
        $cat_faq = get_post_meta( $post->ID, 'actbr_select_faq_class', true );
    if( $cat_faq != 'none' && !empty($cat_faq)){ ?>
        
      <?php $product_name =  get_the_title(); ?>
    <section class="faqs-section">
        <div class="diagonal">
        </div>
        <div class="sec-title light">
            <h2><?php bloginfo( 'name' ); ?> FAQ</h2>
        </div>
         <div class="row">
            <div class="image-column col-lg-4 col-md-12">
                <figure class="image"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/faq-img.png" alt=""></figure>
            </div>
                 <div class="accordion-column col-lg-8 col-md-12">
                <?php
                // the query
                
                /*$the_query = new WP_Query(array(
                    'category_name' => $product_name,
                    'post_status' => 'publish',
                    'posts_per_page' => 100,
                ));*/ 

                $the_query = new WP_Query( array(
                    'post_type' => 'faq_item',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'faq_categories',
                            'field' => 'term_taxonomy_id',
                            'terms' => intval($cat_faq)
                        ),
                    ),
                    'post_status' => 'publish',
                    'posts_per_page' => 100,
                ));
                ?>


               
                            <!--Accordian Box-->
                <ul class="accordion-box">
                    <!--Block-->
                    <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <li class="accordion block">
                        <div class="acc-btn">  <?php the_title(); ?> <span class="icon fa fa-chevron-right"></span></div>
                        <div class="acc-content">
                            <div class="content">
                                <p><?php the_content(); ?> </p>
                            </div>
                        </div>
                    </li>
                    <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                     <p><?php __('No FAQ'); ?></p>
                        <?php endif; unset($the_query) ?>


                    <!--Block-->
                   
                </ul>
            </div>
     </div>
     </section>
     <?php } ?>
      <!-- Sponsor Section -->
    <section class="sponsors-section">
        <div class="auto-container">
            <ul class="row">
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/bloomberg_logo.png" alt=""></a>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/forbes_logo.png" alt=""></a>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/wsj_logo.png" alt=""></a>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/penny_hoarder_logo.png" alt=""></a>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Sponsor Section -->
     <!-- Why Choose us -->
    <section class="why-choose-us">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>WHY YOU SHOULD CHOOSE US</h2>
            </div>

            <div class="row">
                <!-- Feature block Two -->
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <h4><a href="#">How long will it take to get my money?.</a></h4>
                        <div class="text">All Upstart loans offer a fixed interest rate, but individual rates are determined based on various factors including an applicant’s education history, work experience, and credit .</div>
                    </div>
                </div>

                <!-- Feature block Two -->
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <h4><a href="#">How long will it take to get my money?.</a></h4>
                        <div class="text">All Upstart loans offer a fixed interest rate, but individual rates are determined based on various factors including an applicant’s education history, work experience, and credit .</div>
                    </div>
                </div>

                <!-- Feature block Two -->
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <h4><a href="#">How long will it take to get my money?.</a></h4>
                        <div class="text">All Upstart loans offer a fixed interest rate, but individual rates are determined based on various factors including an applicant’s education history, work experience, and credit .</div>
                    </div>
                </div>
            </div>

            <div class="bottom-text">
                <h4>We've helped 1.6 million enterpreneurs <br> just like you start their LLCs.</h4>
            </div>
        </div>
    </section>
    <!-- End Why Choose us -->
    
     <!-- Setup Section -->
    <section class="setup-section">
        <div class="diagonal"></div>
        <div class="sec-title">
        <?php
            echo htmlspecialchars_decode( get_post_meta( $post->ID, 'textarea_setup_box', true),ENT_QUOTES );
        ?>
 
    </section>
    <!-- End Setup Section -->
     <!-- Free Shipping -->
    <div class="free-shipping">
        <div class="diagonal"></div>
        <div class="auto-container">
            <div class="outer-box">
                <div class="shipping-box">
                    <span class="icon fa fa-shuttle-van"></span>
                    <h4>Fast Free shipping <br> <span>On All Web Orders</span></h4>
                </div>

                <div class="shipping-box">
                    <span class="icon icon-check fa fa-check-circle"></span>
                    <h4>Fast Free shipping <br> <span>On All Web Orders</span></h4>
                </div>
            </div>

            <div class="btn-box">
                <a href="#scrolladdtocart" class="sliding-link theme-btn btn-style-one"><span class="btn-title">Check Your Rate</span></a>
            </div>
        </div>
    </div>
    <!-- End Free Shipping -->
    <?php do_action('caber_whatisthis') ?>