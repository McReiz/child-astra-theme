(function($) {
	
	"use strict";
	
	
	//Update Header Style and Scroll to Top
	function headerStyle() {
		if($('.main-header').length){
			var windowpos = $(window).scrollTop();
			var siteHeader = $('.main-header');
			var siteHeader_fix = $('.main-header.fixed');
			var scrollLink = $('.scroll-to-top');
			var sticky_header = $('.main-header');
			if (windowpos > 1) {
				siteHeader.addClass('fixed-header animated slideInDown');
				siteHeader_fix.removeClass('fixed-header animated slideInDown');
				scrollLink.fadeIn(300);
			} else{
				siteHeader.removeClass('fixed-header animated slideInDown');
				scrollLink.fadeOut(300);
			}
		}
	}
	headerStyle();

	function package_items() {
		if($('.package-table').length){
			$(".package-table .opener").on('click', function() {
				$(this).parent('li').toggleClass('active');
				var package_item = $(this).parent('li').attr("data-id");
				var package_item_height =$(this).parent('li').innerHeight();
	            $('.package-inc li[data-value='+package_item+']').outerHeight(package_item_height);
			});
		}
	}
	package_items();


	// Mobile Navigation
	if($('#nav-mobile').length){
		jQuery(function ($) {
		  var $navbar = $('#navbar');
		  var $mobileNav = $('#nav-mobile');
		  
		  $navbar
		    .clone()
		    .removeClass('navbar')
		    .appendTo($mobileNav);
		  
		  $mobileNav.mmenu({
		  	"counters": true,
		  	"extensions": [
	          "theme-dark"
	       ],
		    offCanvas: {
		      position: 'left',
		      zposition: 'front',
		    }
		  });
		});
	}

	//Accordion Box
	if($('.accordion-box').length){
		$(".accordion-box").on('click', '.acc-btn', function() {
			
			var outerBox = $(this).parents('.accordion-box');
			var target = $(this).parents('.accordion');
			
			if($(this).hasClass('active')!==true){
				$(outerBox).find('.accordion .acc-btn').removeClass('active ');
			}
			
			if ($(this).next('.acc-content').is(':visible')){
				return false;
			}else{
				$(this).addClass('active');
				$(outerBox).children('.accordion').removeClass('active-block');
				$(outerBox).find('.accordion').children('.acc-content').slideUp(300);
				target.addClass('active-block');
				$(this).next('.acc-content').slideDown(300);	
			}
		});	
	}

	function show_review_form(){
		$('.show-review-form').click( function(e){
			e.prevendDefault;
			$('#review_form_wrapper2').toggleClass('showreview');

			return false;
		});
	}

	var inputradio = $('#pwzrt_product_options .options-list .choice input[type=radio]');
	var parents = inputradio.parents('.options-list');

	parents.each( function (i){
		var inputs = $(this).children('.choice');

		if(inputs.length < 2){
			inputs.addClass('icon-checked');
			inputs.children('input[type="radio"]').prop('checked', true);
		}
	})

/* ==========================================================================
   When document is Scrollig, do
   ========================================================================== */
	
	$(window).on('scroll', function() {
		headerStyle();
	});

	$(document).ready( function (){
		show_review_form();
	});
	

})(window.jQuery);