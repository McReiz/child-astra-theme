
<div class="woocommerce-checkout">
	<div class="place-order">
<?php wc_get_template( 'checkout/terms.php' ); ?>

<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
</div></div>