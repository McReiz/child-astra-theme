<?php
/*
 * 
 * Child astra by McReiz
 * Extra Taggeds for woocommerce
 */

if ( ! function_exists( 'caber_woocommerce_register_form' ) ) {

	/**
	 * Output the WooCommerce Login Form.
	 *
	 * @param array $args Arguments.
	 */
	function caber_woocommerce_register_form( $args = array() ) {

		$defaults = array(
			'message'  => '',
			'redirect' => '',
			'hidden'   => false,
		);

		$args = wp_parse_args( $args, $defaults );

		wc_get_template( 'global/caber-form-register.php', $args );
	}
}

if ( ! function_exists( 'get_caber_account_information' ) ) {
	function get_caber_account_information($user, $class = ""){
		?>
			<div class="account-details <?php echo $class ?>">
				<div class="header row">
					<h4 class="col-6">Account Informacion: </h4>
					<a class="col-6 text-right" href="<?php echo esc_url( wc_get_account_endpoint_url( 'edit-account' ) ) ?>">Edit Account</a>
				</div>
				<div class="content">
					<div>Hi, <?php echo esc_attr( $user->first_name ); ?> <?php echo esc_attr( $user->last_name ); ?></div>
					<ul>
						<li><strong>Email</strong>: <?php echo $user->user_email ?></li>
						<li><strong>Password:</strong> hidden</li>
					</ul>	
				</div>
			</div>
		<?php
	}
}