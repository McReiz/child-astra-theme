<?php 
class shortcode_product_categories{
	public function __construct(){
		add_shortcode('get_categories_product', array($this,'render_shortcode'));
	}

	public function render_shortcode($atts){
		$atts = shortcode_atts( array(
			'id' => 'none',
			'view_more_text' => 'View All',
			'description' => 'none',
			'extra_class' => ''
		),$atts);

		ob_start();

		if($atts['id'] == 'none'):
		?>
			<div class="category-container <?php echo $atts['extra_class'] ?>">
				<div class="category-container__thumbnail"></div>
				<h2 class="category-container__title">Category Name here</h2>
				<div class="category-container__divisor <?php echo $atts['extra_class'] ?>"></div>
				<div class="category-container__content">
					<ul>
						<li><a href="#">test</a></li>
						<li><a href="#">test</a></li>
						<li><a href="#">test</a></li>
					</ul>
				</div>
				<a href="#" class="category-container__more"><?php echo $atts['view_more_text'] ?></a>
			</div>
		<?php 
		else:
			$taxonomy_name = 'product_cat';
			$category = get_term( $atts['id'], $taxonomy_name );

			if($category){

				$category_children = get_term_children($category->term_id,'product_cat');
				$cat_meta = get_term_meta($category->term_id);
				?>
				<div class="category-container <?php echo $atts['extra_class'] ?>">
					<div class="category-container__thumbnail">
						<?php echo wp_get_attachment_image($cat_meta['thumbnail_id'][0],'thumbnail') ?>
					</div>
					<h2 class="category-container__title"><?php echo $category->name ?></h2>
					<div class="category-container__divisor <?php echo $atts['extra_class'] ?>"></div>
					<div class="category-container__content">
						<?php if($atts['description'] == 'none'){ ?>
							<ul>
								<?php foreach ($category_children as $child){
									$term = get_term_by( 'id', $child, $taxonomy_name );
									echo '<li>';
									echo '<a href="'.get_term_link($child).'">';
									echo $term->name;
									echo '</a>';
									echo '</li>';
								} ?>
							</ul>
						<?php }else{ ?>
							<?php echo $category->description ?>
						<?php } ?>
					</div>
					<a href="<?php echo get_term_link($category) ?>" class="category-container__more"><?php echo $atts['view_more_text'] ?></a>
				</div>
			<?php }else{
				echo '<div class="category-container">insert valid id</div>';
			}
		endif;

		$result = ob_get_contents();
		ob_end_clean();

		if(!is_admin()) 
			return $result;
	}
}

$run_product_categories = new shortcode_product_categories();
?>