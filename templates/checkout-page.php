<?php 
/**
 * Template name: Checkout page
 */
?>
<!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,600;0,700;0,800;0,900;1,300;1,400;1,500&display=swap" rel="stylesheet">

</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>

<?php astra_body_top(); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>

	<?php astra_header_before(); ?>

	<div class="main-header-bar-wrap">
		<div class="main-header-bar">
			<?php astra_main_header_bar_top(); ?>
			<div class="ast-container">

				<div class="ast-flex main-header-container header-checkout flex-lg-row justify-lg-content-between align-items-center">
					<div class="link-top-back col-6 col-lg-4">
						<div class="ast-site-identity" itemscope="itemscope" itemtype="https://schema.org/Organization">
							<?php astra_logo(); ?>
						</div>
					</div>
					<div class="site-branding col-12 col-lg-4">
						<div class="ast-site-identity" itemscope="itemscope" itemtype="https://schema.org/Organization">
							<h2 class="Checkout-title"><?php echo get_the_title( ); ?></h2>
						</div>
					</div>
					<div class="nav-icons col-6 col-lg-4">
						<i class="fa fa-lock"></i>
					</div>
				</div><!-- Main Header Container -->
			</div><!-- ast-row -->
			<?php astra_main_header_bar_bottom(); ?>
		</div> <!-- Main Header Bar -->
	</div> <!-- Main Header Bar Wrap -->


	<?php astra_header_after(); ?>

	<?php astra_content_before(); ?>

	<div id="content" class="site-content">

		<div class="ast-container page-checkout">

		<?php astra_content_top(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>