<?php 
/**
 * 
 */
class Caber_Woo_Functions
{
	public function init() {
		//add_action( 'wp_loaded', array( __CLASS__, 'caber_process_registration' ), 20 );
		
		/* no cart */
		add_filter('add_to_cart_redirect', 'caber_redirect_add_to_cart');

		add_action( 'woocommerce_register_form', array($this, 'caber_extra_register_fields' ));
		add_action( 'woocommerce_register_post', array($this,'caber_validate_extra_register_fields'), 10, 3 );
		add_action( 'woocommerce_created_customer', array($this,'caber_save_extra_register_fields' ));
	}
	/*
	public static function caber_process_registration() {
		$nonce_value = isset( $_POST['_wpnonce'] ) ? wp_unslash( $_POST['_wpnonce'] ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
		$nonce_value = isset( $_POST['caber-woocommerce-register-nonce'] ) ? wp_unslash( $_POST['caber-woocommerce-register-nonce'] ) : $nonce_value; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

		if ( isset( $_POST['register'], $_POST['email'], $_POST['account_first_name'], $_POST['account_last_name'], $_POST['account_company_name'] ) && wp_verify_nonce( $nonce_value, 'caber-woocommerce-register' ) ) {
			$username = 'no' === get_option( 'woocommerce_registration_generate_username' ) && isset( $_POST['username'] ) ? wp_unslash( $_POST['username'] ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
			$password = 'no' === get_option( 'woocommerce_registration_generate_password' ) && isset( $_POST['password'] ) ? $_POST['password'] : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
			$email    = wp_unslash( $_POST['email'] ); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized


			$caber_args = array(
				'first_name' => sanitize_text_field($_POST['account_first_name']),
				'last_name' => sanitize_text_field($_POST['account_last_name']),
				'company_name' => sanitize_text_field($_POST['account_company_name'])
			);

			try {
				$validation_error  = new WP_Error();
				$validation_error  = apply_filters( 'woocommerce_process_registration_errors', 
					$validation_error, 
					$username, 
					$password, 
					$email, 
					$caber_args['first_name'], 
					$caber_args['last_name'], 
					$caber_args['company_name'] );

				if ( empty( $_POST['account_first_name'] ) ) {
					return new WP_Error( 'registration-error-invalid-text', __( 'Please provide a valid first name .', 'woocommerce' ) );
				}
				if ( empty( $_POST['account_last_name'] ) ) {
					return new WP_Error( 'registration-error-invalid-text', __( 'Please provide a valid last name .', 'woocommerce' ) );
				}
				if ( empty( $_POST['account_company_name'] ) ) {
					return new WP_Error( 'registration-error-invalid-text', __( 'Please provide a valid company name.', 'woocommerce' ) );
				}


				$validation_errors = $validation_error->get_error_messages();

				if ( 1 === count( $validation_errors ) ) {
					throw new Exception( $validation_error->get_error_message() );
				} elseif ( $validation_errors ) {
					foreach ( $validation_errors as $message ) {
						wc_add_notice( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $message, 'error' );
					}
					throw new Exception();
				}

				$new_customer = wc_create_new_customer( sanitize_email( $email ), wc_clean( $username ), $password, $caber_args );

				if ( is_wp_error( $new_customer ) ) {
					throw new Exception( $new_customer->get_error_message() );
				}

				if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) ) {
					wc_add_notice( __( 'Your account was created successfully and a password has been sent to your email address.', 'woocommerce' ) );
				} else {
					wc_add_notice( __( 'Your account was created successfully. Your login details have been sent to your email address.', 'woocommerce' ) );
				}

				// Only redirect after a forced login - otherwise output a success notice.
				if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $new_customer ) ) {
					wc_set_customer_auth_cookie( $new_customer );

					if ( ! empty( $_POST['redirect'] ) ) {
						$redirect = wp_sanitize_redirect( wp_unslash( $_POST['redirect'] ) ); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
					} elseif ( wc_get_raw_referer() ) {
						$redirect = wc_get_raw_referer();
					} else {
						$redirect = wc_get_page_permalink( 'myaccount' );
					}

					wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_registration_redirect', $redirect ), wc_get_page_permalink( 'myaccount' ) ) ); //phpcs:ignore WordPress.Security.SafeRedirect.wp_redirect_wp_redirect
					exit;
				}
			} catch ( Exception $e ) {
				if ( $e->getMessage() ) {
					wc_add_notice( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $e->getMessage(), 'error' );
			}
		}
	}
	*/

	/**
	* register fields Validating.
	*/
	public function caber_validate_extra_register_fields( $username, $email, $validation_errors ) {
		if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
			$validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
		}

		if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
			$validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
		}

		if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
			$validation_errors->add( 'billing_company_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
		}

		return $validation_errors;
	}

	public function caber_save_extra_register_fields( $customer_id ) {
      if ( isset( $_POST['billing_first_name'] ) ) {
             //First name field which is by default
             update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
             // First name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
             // Last name field which is by default
             update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }
      if ( isset( $_POST['billing_company'] ) ) {
             // Last name field which is by default
             //update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
      }
	}

	static function caber_extra_register_fields() {?>
		<p class="form-row form-row-first">
			<label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
		</p>
		<p class="form-row form-row-last">
			<label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="reg_billing_company"><?php _e( 'Company Name', 'woocommerce' ); ?><span class="required">*</span></label>
			<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_company" id="reg_billing_company" value="<?php if ( ! empty( $_POST['billing_company'] ) ) esc_attr_e( $_POST['billing_company'] ); ?>" />
		</p>
		<div class="clear"></div>
		<?php
  	}


    function caber_redirect_add_to_cart() {
        global $woocommerce;
        $cw_redirect_url_checkout = $woocommerce->cart->get_checkout_url();
        return $cw_redirect_url_checkout;
    }
}


$caber_woo_functions = new Caber_Woo_Functions();
$caber_woo_functions->init();
?>