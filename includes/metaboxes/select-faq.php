<?php 



/* add meta box */
add_action( 'add_meta_boxes', 'actbr_select_faq_add_post_meta_boxes' );
function actbr_select_faq_add_post_meta_boxes() {

  add_meta_box(
    'actbr-select-faq-class',      // Unique ID
    esc_html__( 'Add Faq', 'child-theme-astra' ),    // Title
    'actbr_select_faq_class_meta_box',   // Callback function
    'product',         // Admin page (or post type)
    'side',         // Context
    'default'         // Priority
  );
}

/* Display the post meta box. */
function actbr_select_faq_class_meta_box( $post ) { ?>
	<?php 
	$args = array(
               'taxonomy' => 'faq_categories',
               'orderby' => 'name',
               'order'   => 'ASC'
           );

   $fcats = get_categories($args);

  wp_nonce_field( basename( __FILE__ ), 'actbr_select_faq_class_nonce' ); ?>

  <p>
    <label for="actbr-select-faq-class"><?php _e( "select a category of FAQ.", 'child-theme-astra' ); ?></label>
    <br />
    
    <?php class_erz_snippets::select_input_cat("actbr-select-faq-class",$fcats,get_post_meta( $post->ID, 'actbr_select_faq_class', true )); ?>
  </p>
<?php }

// Saving
add_action('save_post', 'actbr_select_faq_save_postdata');
function actbr_select_faq_save_postdata($post_id)
{
  /*
     * We need to verify this came from the our screen and with proper authorization,
     * because save_post can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['actbr_select_faq_class_nonce'] ) ) {
        return $post_id;
    }

    $nonce = $_POST['actbr_select_faq_class_nonce'];

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $nonce, basename( __FILE__ ) ) ) {
        return $post_id;
    }

    /*
     * If this is an autosave, our form has not been submitted,
     * so we don't want to do anything.
     */
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }

    // Check the user's permissions.
    /*if ( 'product' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }*/

    /* OK, it's safe for us to save the data now. */

    // Sanitize the user input.
    $mydata = sanitize_text_field( $_POST['actbr-select-faq-class'] );

    // Update the meta field.
    update_post_meta( $post_id, 'actbr_select_faq_class', $mydata );
}
