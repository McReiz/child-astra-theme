<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );?>

    <!--<img src="<?php  //echo $image[0]; ?>" data-id="<?php //echo $loop->post->ID; ?>"> -->
    <!-- Banner Section-->
    <section class="banner-section" style="background-image: url(<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
        <div class="auto-container">
            <div class="content-box">
                <div class="inner-column">
                    <div class="content-box">
                        <h2><?php echo esc_html( get_the_title() ); ?></h2>
                       <?php
                   

global $post;

$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

if ( ! $short_description ) {
	return;
}

?>
<p><?php echo $short_description; // WPCS: XSS ok. ?></p>
                        <a href="#scrolladdtocart" class="sliding-link theme-btn btn-style-one"><span class="btn-title">Get Started</span></a>
                        <div class="text"><span class="fa fa-lock"></span> 100% Money back guarantee</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Section-->
    
    <!-- End Package Section -->