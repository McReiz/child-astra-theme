<?php 

/*get_comment_meta( $comment_id, $meta_key, $single = false );
add_comment_meta($comment_id, $meta_key, $meta_value, $unique = false );
update_comment_meta($comment_id, $meta_key, $meta_value, $unique = false );
delete_comment_meta( $comment_id, $meta_key, $single = false );*/

 // This is where you'd do the validations:

/**
 * 
 */
class CustomFieldInReviewForm
{
	
	function __construct()
	{
		add_filter( 'preprocess_comment', array($this, 'verify_comment_meta_data' ) );
		add_action( 'comment_post', array($this, 'save_comment_meta_data' ) );
		add_filter( 'get_comment_author_link', array($this, 'attach_city_to_author' ) );
	}


	function verify_comment_meta_data( $commentdata ) {
		if ( ! isset( $_POST['subject'] ) )
			wp_die( __( 'Error: please fill the required field (subject).' ) );
		return $commentdata;
	}

	// And save the comment meta:


	function save_comment_meta_data( $comment_id ) {
		add_comment_meta( $comment_id, 'subject', $_POST[ 'subject' ] );
	}

	// Retrieve and display comment meta:


	function attach_city_to_author( $author ) {
		$subject = get_comment_meta( get_comment_ID(), 'subject', true );
		if ( $subject )
			$author .= " ($subject)";

		return $author;
	}
}

$run_cfirf = new CustomFieldInReviewForm();

?>