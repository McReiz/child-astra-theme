<?php
/**
 * Child Theme Astra Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Child Theme Astra
 * @since 1.0
 */


/**
 * Define Constants
 */
define( 'CHILD_THEME_CHILD_THEME_ASTRA_VERSION', '1.0' );

/**
 * Enqueue styles 
 */
function child_enqueue_styles() {
    
	wp_enqueue_style( 'child-theme-astra-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_CHILD_THEME_ASTRA_VERSION, 'all' );
    $version_caber = date('dmoYHis');
	wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,600;0,700;0,800;0,900;1,300;1,400;1,500&display=swap');

	wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/assets/css/animate.css', array(), $version_caber, 'all');
  wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.css', array(), $version_caber, 'all');
  wp_enqueue_style( 'fontawesome', get_stylesheet_directory_uri() . '/assets/css/fontawesome-all.css', array(), $version_caber, 'all');
  wp_enqueue_style( 'mmenu', get_stylesheet_directory_uri() . '/assets/css/mmenu.css', array(), $version_caber, 'all');
  wp_enqueue_style( 'responsive-child-theme', get_stylesheet_directory_uri() . '/assets/css/responsive.css', array(), $version_caber, 'all');
  
  wp_deregister_style('ivole-reviews-grid');
    wp_dequeue_style('ivole-reviews-grid');
   wp_enqueue_style( 'ivole-sm-reviews-grid', get_stylesheet_directory_uri() . '/assets/css/customer-reviews-woocommerce/reviews-grid.css', array(), $version_caber);
  
  wp_enqueue_style( 'main-style-child-theme', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), $version_caber, 'all');
  
  wp_enqueue_script( 'apper-script', get_stylesheet_directory_uri() . '/assets/js/appear.js', array (), $version_caber, true);
  wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array (), $version_caber, true);
  wp_enqueue_script( 'isotope', get_stylesheet_directory_uri() . '/assets/js/isotope.js', array (), $version_caber, true);
  wp_enqueue_script( 'jquery-countdown', get_stylesheet_directory_uri() . '/assets/js/jquery.countdown.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'jquery-fancybox', get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.js', array ( 'jquery' ), $version_caber, true);
  //wp_enqueue_script( 'jquery-1-1', get_stylesheet_directory_uri() . '/assets/js/jquery.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'mapscript', get_stylesheet_directory_uri() . '/assets/js/map-script.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'mixitup', get_stylesheet_directory_uri() . '/assets/js/mixitup.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'mmenu', get_stylesheet_directory_uri() . '/assets/js/mmenu.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'mmenu-polyfills', get_stylesheet_directory_uri() . '/assets/js/mmenu.polyfills.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'owl-script', get_stylesheet_directory_uri() . '/assets/js/owl.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'parallax', get_stylesheet_directory_uri() . '/assets/js/parallax.min.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'popper', get_stylesheet_directory_uri() . '/assets/js/popper.min.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'main-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'sweetalert2', get_stylesheet_directory_uri() . '/assets/js/sweetalert2.min.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'typed', get_stylesheet_directory_uri() . '/assets/js/typed.js', array ( 'jquery' ), $version_caber, true);
  wp_enqueue_script( 'wow', get_stylesheet_directory_uri() . '/assets/js/wow.js', array ( 'jquery' ), $version_caber, true);
    
    
   
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

function fit_footer_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer widgets', 'textdomain' ),
        'id'            => 'footer-widget-set',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<section id="%1$s" class="col-lg-4 col-md-4 col-sm-12">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'fit_footer_widgets_init' );


function the_reviews_two(){
  include(  get_stylesheet_directory() . '/woocommerce/single-product-reviews2.php' );
}
add_action('caber_reviews_variation','woocommerce_output_all_notices',1);
add_action('caber_reviews_variation','the_reviews_two', 2);

/* includes */
include(  get_stylesheet_directory() . '/includes/class-erz-snippets.php' );
include(  get_stylesheet_directory() . '/includes/custom-posts.php' );
include(  get_stylesheet_directory() . '/includes/metaboxes.php' );
include(  get_stylesheet_directory() . '/includes/shortcodes.php' );
include(  get_stylesheet_directory() . '/includes/custom-reviews/subject-field.php' );

if ( class_exists( 'WooCommerce' ) ){
  include(  get_stylesheet_directory() . '/includes/woocommerce-functions.php' );
  include(  get_stylesheet_directory() . '/includes/woocommerce-taggeds.php' );
}