<?php

function cptui_register_my_cpts_faq_item() {

	/**
	 * Post Type: Faq items.
	 */

	$labels = [
		"name" => __( "Faq items", "child-theme-astra" ),
		"singular_name" => __( "Faq item", "child-theme-astra" ),
		"menu_name" => __( "Products Faqs", "child-theme-astra" ),
		"all_items" => __( "All items", "child-theme-astra" ),
		"add_new" => __( "Add new FAQ", "child-theme-astra" ),
	];

	$args = [
		"label" => __( "Faq items", "child-theme-astra" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"delete_with_user" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "faq_item", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "faq_item", $args );
}

add_action( 'init', 'cptui_register_my_cpts_faq_item' );

function cptui_register_my_taxes_faq_categories() {

	/**
	 * Taxonomy: faq categories.
	 */

	$labels = [
		"name" => __( "faq categories", "child-theme-astra" ),
		"singular_name" => __( "faq category", "child-theme-astra" ),
	];

	$args = [
		"label" => __( "faq categories", "child-theme-astra" ),
		"labels" => $labels,
		"public" => false,
		"publicly_queryable" => false,
		"hierarchical" => true,
		"show_ui" => true,
		"query_var" => false,
		"rewrite" => [ 'slug' => 'faq_categories', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"show_in_quick_edit" => true,
		//"meta_box_cb" => post_categories_meta_box()
			];
	register_taxonomy( "faq_categories", [ "faq_item" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_faq_categories' );

?>