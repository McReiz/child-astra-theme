<?php 

	/**
	 * Setup textarea
	 */
	class caber_SetupTextarea
	{
		
		private $_nonce_name = "setuparea_box_nonce";
		private $_id_name = "setuparea-box";
		private $_post_type = "product";
		private $_title = "Setup Section";

		// Inputs name=""
		private $_textarea_setup = "textarea_setup_box";

		function __construct()
		{
			add_action('add_meta_boxes', array( $this, 'metabox_setup' ));
			add_action('save_post', array( $this, 'metabox_save_post' ));
		}

		public function metabox_setup(){
			add_meta_box(
				$this->_id_name,      // Unique ID
				esc_html__( $this->_title, 'child-theme-astra' ),    // Title
				array($this, 'metabox_section'),   // Callback function
				$this->_post_type,         // Admin page (or post type)
				'advanced',         // Context
				'default'         // Priority
			);
		}

		/*
			save data
		*/
		public function metabox_save_post($post_id){
			/*
			 * We need to verify this came from the our screen and with proper authorization,
			 * because save_post can be triggered at other times.
			 */

			// Check if our nonce is set.
			if ( ! isset( $_POST[ $this->_nonce_name ] ) ) {
				return $post_id;
			}

			$nonce = $_POST[ $this->_nonce_name ];

			// Verify that the nonce is valid.
			if ( ! wp_verify_nonce( $nonce, basename( __FILE__ ) ) ) {
				return $post_id;
			}

			/*
			* If this is an autosave, our form has not been submitted,
			* so we don't want to do anything.
			*/
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return $post_id;
			}

			/* OK, it's safe for us to save the data now. */

			// Sanitize the user input.
			$mydata = sanitize_text_field( esc_html( $_POST[ $this->_textarea_setup ] ));

			
			// Update the meta field.
			update_post_meta( $post_id, $this->_textarea_setup, $mydata );
		}

		/* 
			render
		*/
		public function metabox_section($post){
			wp_nonce_field( basename( __FILE__ ),  $this->_nonce_name);
			$content = htmlspecialchars_decode(get_post_meta( $post->ID, $this->_textarea_setup, true),ENT_QUOTES);
			
			wp_editor($content,$this->_textarea_setup);
		}

	}

	$run = new caber_SetupTextarea();