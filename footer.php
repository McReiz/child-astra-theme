<?php astra_content_bottom(); ?>
</div> <!-- ast-container -->
</div><!-- #content -->
<!-- Main Footer -->
    <footer class="main-footer">
        <div class="auto-container">

            <!-- Footer Links -->
            <div class="footer-links row">
                <?php if ( is_active_sidebar( 'footer-widget-set' ) ) { dynamic_sidebar('footer-widget-set'); } ?>
            </div>


            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="copyright-text">Copyright © 2020 Jifstartup. All rights reserved.</div>

                <ul class="social-links">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                </ul>
            </div>
        </div>
    </footer>
    <!-- End Main Footer -->
    <?php astra_body_bottom(); ?>

	<?php wp_footer(); ?>
</div>