<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
global $post;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

?>


<script>
    (function ($){
        $(".sliding-link").click(function(e) {
            e.preventDefault();
            var aid = $(this).attr("href");
            $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
        });
    })(jQuery);
</script>

<section class="package-section ivole-reviews-grid"  id="productDiv-<?php echo $product->get_id() ?>">
    <div class="diagonal"></div>
    <div class="auto-container summary summary-caber">
    	<div class="row no-gutters">
    	
			<?php
			$short_description = apply_filters( 'woocommerce_short_description', $post->post_content );
				if ( ! $short_description ) {
					return;
				}
			?>
			<?php echo $short_description; // WPCS: XSS ok. ?> 
		</div>

		<div class="wrap-container-content-products">
	    	<?php if ( $product->is_in_stock() ) : ?>

				<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

				<form class="cart wrap-options" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                        <div class="price-field">
                            <div class="total-label">
                                <span>Total</span>
                            </div>
                            <div class="total-count">
                                <?php echo '<span class="price">'.$product->get_price_html().'</span>'; // PHPCS:Ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                            </div>
                        </div>
                        <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
					<?php
						do_action( 'woocommerce_before_add_to_cart_quantity' );
                        ?>
                            
                        <?php

						// https://wordpress.org/support/topic/uncaught-typeerror-cant-access-property-textcontent/
						woocommerce_quantity_input(
							array(
								'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
								'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
								'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
							)
						);

						do_action( 'woocommerce_after_add_to_cart_quantity' );
					?>
                    <div class="submit-button-panel">
                        <div class="submit-button-panel__left"></div>
                        <div class="submit-button-panel__right">
                            <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt submit-button-panel__button"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                        </div>
                    </div>
					

					<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
				    </form>

				<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

			<?php endif; ?>
    	</div>
    </div>
</section>

 <!-- Features Section -->
    <section class="features-section">
        <div class="diagonal"></div>
        <div class="sec-title text-center">
            <h2>HOW IT WORKS</h2>
            <span class="sub-title">Fase and easy</span>
        </div>
        <div class="row">
            <!-- Service Block -->
            <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-clock.svg" alt=""></div>
                    <h4>Add to cart & purchase</h4>
                    <div class="text">99% of applicants get money just 1 business day after accepting their loans*</div>
                </div>
            </div>

            <!-- Service Block -->
            <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-bag.svg" alt=""></div>
                    <h4>Tell us about your business</h4>
                    <div class="text">99% of applicants get money just 1 business day after accepting their loans*</div>
                </div>
            </div>

            <!-- Service Block -->
            <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-calendar.png" alt=""></div>
                    <h4>we do the best</h4>
                    <div class="text">99% of applicants get money just 1 business day after accepting their loans*</div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Features Section -->
 <!-- Services Section -->
    <section class="services-section">
        <div class="diagonal"></div>
        <div class="sec-title">
            <h2>FILE LLC IN TAXES<br> FOR ANY BUSINESS</h2>
        </div>
        <div class="outer-box clearfix">
            <!-- Service Block -->
            <div class="column">
                <div class="service-block block-one" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/moving_loans.jpg);">
                    <div class="text-box"><h3>Moving Loans</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
                <div class="service-block block-two" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/credit_card_consolidation.jpg);">
                    <div class="text-box"><h3>Credit Card Consolidation</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
            </div>

            <!-- Service Block -->
            <div class="column">
                <div class="service-block block-two" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/home_improvement.jpg);">
                    <div class="text-box"><h3>Home Improvement</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
                <div class="service-block block-one" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/debt_consolidation.jpg);">
                    <div class="text-box"><h3>Debt Consolidation</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
            </div>

            <!-- Service Block -->
            <div class="column">
                <div class="service-block block-one" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/medical_loans.jpg);">
                    <div class="text-box"><h3>Medical Loans</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
                <div class="service-block block-two" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/wedding_loans.jpg);">
                    <div class="text-box"><h3>Wedding Loans</h3></div>
                    <a href="#" class="absolute-link"></a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services Section -->
 <!-- Testimonials Section -->
 
 
   <!-- Testimonials Section -->
   <section class="testimonials-section">
            <div class="diagonal"></div>
            <div class="sec-title text-right">
                <h2>WHAT OUR NEW BUSINESS <br>OWNERS SAY</h2>
            </div>

            <div class="outer-box">
                <div class="upper-title">
                    <h4>Excellent</h4>
                    <div class="rating-star">
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </div>
                </div>

                <!-- Business Info -->
                <div class="businessinfo">
              <?php
              $rating  = $product->get_average_rating();
             $count   = $product->get_rating_count();

//echo wc_get_rating_html( $rating, $count );
               ?>
                    <a href="#"><span>Rated  <b><?php echo $rating; ?></b> out of 5 based on <b class="text_underline"><?php echo $count; ?> reviews </b></a><span class="palito-separador"> | </span><a href="#" class="show-review-form"><b class="text_underline">Add you Review</b></a></span>
                </div>

                <div class="wrapper-add-reviews">
                    <?php do_action('caber_reviews_variation') ?>
                </div>

                <div class="wrapper-reviews">
                    <div class="row custom-row no-gutters ivole-reviews-grid">
                       
                                <?php   echo do_shortcode( '[cusrev_reviews_grid count="100" products="'.$product->get_id().'"]' ); ?>
                             
                    </div>
                </div>
            </div>

            <div class="btn-box">
                <a href="#scrolladdtocart"  class="sliding-link theme-btn btn-style-one"><span class="btn-title">Get Started</span></a>
            </div>
        </section>
        <!-- End Testimonials Section -->
    <!-- End Testimonials Section -->
    <?php 
        $cat_faq = get_post_meta( $post->ID, 'actbr_select_faq_class', true );
    if( $cat_faq != 'none' && !empty($cat_faq)){ ?>
        
      <?php $product_name =  get_the_title(); ?>
    <section class="faqs-section">
        <div class="diagonal">
        </div>
        <div class="sec-title light">
            <h2><?php bloginfo( 'name' ); ?> FAQ</h2>
        </div>
         <div class="row">
            <div class="image-column col-lg-4 col-md-12">
                <figure class="image"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/resource/faq-img.png" alt=""></figure>
            </div>
                 <div class="accordion-column col-lg-8 col-md-12">
                <?php
                // the query
                
                /*$the_query = new WP_Query(array(
                    'category_name' => $product_name,
                    'post_status' => 'publish',
                    'posts_per_page' => 100,
                ));*/ 

                $the_query = new WP_Query( array(
                    'post_type' => 'faq_item',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'faq_categories',
                            'field' => 'term_taxonomy_id',
                            'terms' => intval($cat_faq)
                        ),
                    ),
                    'post_status' => 'publish',
                    'posts_per_page' => 100,
                ));
                ?>


               
                            <!--Accordian Box-->
                <ul class="accordion-box">
                    <!--Block-->
                    <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <li class="accordion block">
                        <div class="acc-btn">  <?php the_title(); ?> <span class="icon fa fa-chevron-right"></span></div>
                        <div class="acc-content">
                            <div class="content">
                                <p><?php the_content(); ?> </p>
                            </div>
                        </div>
                    </li>
                    <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                     <p><?php __('No FAQ'); ?></p>
                        <?php endif; unset($the_query) ?>


                    <!--Block-->
                   
                </ul>
            </div>
     </div>
     </section>
     <?php } ?>
      <!-- Sponsor Section -->
    <section class="sponsors-section">
        <div class="auto-container">
            <ul class="row">
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/bloomberg_logo.png" alt=""></a>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/forbes_logo.png" alt=""></a>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/wsj_logo.png" alt=""></a>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/clients/penny_hoarder_logo.png" alt=""></a>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Sponsor Section -->
     <!-- Why Choose us -->
    <section class="why-choose-us">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>WHY YOU SHOULD CHOOSE US</h2>
            </div>

            <div class="row">
                <!-- Feature block Two -->
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <h4><a href="#">How long will it take to get my money?.</a></h4>
                        <div class="text">All Upstart loans offer a fixed interest rate, but individual rates are determined based on various factors including an applicant’s education history, work experience, and credit .</div>
                    </div>
                </div>

                <!-- Feature block Two -->
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <h4><a href="#">How long will it take to get my money?.</a></h4>
                        <div class="text">All Upstart loans offer a fixed interest rate, but individual rates are determined based on various factors including an applicant’s education history, work experience, and credit .</div>
                    </div>
                </div>

                <!-- Feature block Two -->
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <h4><a href="#">How long will it take to get my money?.</a></h4>
                        <div class="text">All Upstart loans offer a fixed interest rate, but individual rates are determined based on various factors including an applicant’s education history, work experience, and credit .</div>
                    </div>
                </div>
            </div>

            <div class="bottom-text">
                <h4>We've helped 1.6 million enterpreneurs <br> just like you start their LLCs.</h4>
            </div>
        </div>
    </section>
    <!-- End Why Choose us -->
    
     <!-- Setup Section -->
    <section class="setup-section">
        <div class="diagonal"></div>
        <div class="sec-title">
        <?php
            echo htmlspecialchars_decode( get_post_meta( $post->ID, 'textarea_setup_box', true),ENT_QUOTES );
        ?>
 
    </section>
    <!-- End Setup Section -->
     <!-- Free Shipping -->
    <div class="free-shipping">
        <div class="diagonal"></div>
        <div class="auto-container">
            <div class="outer-box">
                <div class="shipping-box">
                    <span class="icon fa fa-shuttle-van"></span>
                    <h4>Fast Free shipping <br> <span>On All Web Orders</span></h4>
                </div>

                <div class="shipping-box">
                    <span class="icon icon-check fa fa-check-circle"></span>
                    <h4>Fast Free shipping <br> <span>On All Web Orders</span></h4>
                </div>
            </div>

            <div class="btn-box">
                <a href="#scrolladdtocart" class="sliding-link theme-btn btn-style-one"><span class="btn-title">Check Your Rate</span></a>
            </div>
        </div>
    </div>
    <!-- End Free Shipping -->
    <?php do_action('caber_whatisthis') ?>